package com.manko.task03;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SenderSMS {

    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "ACdd453758233e3a00a54ff14a2e445c59";
    public static final String AUTH_TOKEN = "c2a26480c843b15c26f8dabcb3ba13af";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber("+380633218365"),
                new PhoneNumber("+12015828435"), str).create();
    }
}
